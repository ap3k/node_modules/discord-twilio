# Discord Twilio 

NPM repo: https://www.npmjs.com/package/discord-twilio

Git repo: https://gitlab.com/ap3k/node_modules/discord-twilio

Issues and suggestions: https://gitlab.com/ap3k/node_modules/discord-twilio/issues

Send SMS through Twilio to user who is mentioned with @name in Discord

## Usage
> Read the Source Luke!

1) Make Twilio account
2) Rename `config.example.json` to `config.json` and change needed parameters in there
3) Start with `node index.js`
4) Mention someone listed in config.json with @somename and SMS will be sent to them

## Notices
> Have not tested with @here and @everyone