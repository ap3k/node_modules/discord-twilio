const config = require('./config.json');

const Discord = require('discord.js');
var twilio = require('twilio');

// Initialize Discord client
const client = new Discord.Client();

var twilioClient = new twilio(config.TWILIO_ACCOUNT_SID, config.TWILIO_AUTH_TOKEN);

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

const SMS_USERS = config.SMS_USERS;

client.on('message', msg => {
    let users = findUsers(msg.mentions.users);
    users.forEach(user => {
        twilioClient.messages.create({
            body: cleanBody(msg.content),
            to: SMS_USERS[user.id].phone,
            from: config.TWILIO_SMS_FROM
        })
        .then((message) => {
            msg.channel.send('Notification SMS sent to ' + user.username);    
        })
        .done();
    });
});

client.login(config.DISCORD_LOGIN_TOKEN);


function findUsers(mentions) {
    let users = []
    Object.keys(SMS_USERS).some(key => {
        let smsUser = SMS_USERS[key];
        let mentionedUser = mentions.get(smsUser.id);
        if (mentionedUser) {
            users.push(mentionedUser);
        }
    });
    return users
}

function cleanBody(content) {
    Object.keys(SMS_USERS).forEach(key => {
        let user = SMS_USERS[key];
        content = content.replace('<@' + user.id + '>', '@' + user.name);
    });
    return truncate(content);
};

function truncate(string, len = 65) {
    if (string.length > len) {
        return string.substring(0, len) + '...';
    }        
    return string;
};